# README #

This is an application utilizing MVC architecture along with ASP.NET and Entity Framework. It is based on a tutorial by Microsoft. The application has functionality which makes it hostable on Azure. I tried to also implement the DayPilot calendar to pick a dates but apparently it proved to be too complex.

Originally the project is from 2016 and I have made few improvements after it. This is now reuploaded after redacting my real name from several locations of the code. 

### Models ###

## Student ##

The base model for interaction. A student has his/her unique StudentID which is tied into Course, Event, Enrollment and Attendance. All enrollments and attendances are collected into corresponding ICollections. 

* public int StudentID 
* public string FirstName 
* public string LastName 
* public string Email 
* public virtual ICollection<Enrollment> Enrollments
* public virtual ICollection<Attendance> Attendances 

## Course ##

Model for a Course that is enrolled by a Student. CourseID is an unique identifier which acts as a key. All enrollments are stored into an ICollection. CourseID is tied into Enrollment via CourseID.

* public String title
* public int CourseID 
* public int Credits
* public virtual ICollection <Enrollment> Enrollments

## Event ##

An event that can be attended by a student (a party, special event for example). EventID is an unique identifier for each event and it acts like a key. EventID is tied into an Attendance.

* public int EventID
* public String Title
* public String Location
* public String Description
* public DateTime StartDate
* public DateTime EndDate

## Enrollment ##

An enrollment into a course, where CourseID and StudentID are used along with the model's own EnrollmentID. 

* public int EnrollmentID 
* public int CourseID 
* public int StudentID
* public Grade? Grade
* public virtual Course course
* public virtual Student student

## Attendance  ##

An attendance into an Event (perhaps this model could be also modified to replace the Enrollment model). Uses Event and Student models as virtual properties. I also notices that an Enrollment has an unique ID whereas an attendance doesn't have one. Inconsistent! 

* public int EventID
* public boolean isAttending
* public int StudentID
* public Virtual Event 
* public Virtual Student

### Who do I talk to? ###

Insill @ IRCnet