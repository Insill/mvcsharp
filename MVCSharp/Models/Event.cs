﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVCSharp.Models
{
    public class Event
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EventID { get; set; }
        [Required(ErrorMessage="The title cannot be blank")]
        public string Title { get; set; }
        [Required(ErrorMessage="The location cannot be blank")]
        public string Location { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage ="The start date cannot be blank")]
        public DateTime startDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage ="The end date cannot be blank")]
        public DateTime endDate { get; set; }
        [StringLength(250)]
        [DataType(DataType.Text)]
        public String Description { get; set; }
    }

}