namespace MVCSharp.Migrations
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using MVCSharp.DAL;
    using System.Data.SqlClient;
    internal sealed class Configuration : DbMigrationsConfiguration<MVCSharp.DAL.SchoolContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(SchoolContext context)
        {
            
            var students = new List<Student>
            {

                new Student
                { FirstName="John",LastName="Doe",Email="johndoe@gmail.com" },
                new Student
                {FirstName="Olli", LastName="Opiskelija",Email="opiskelija_o@hotmail.com" },
                new Student
                {FirstName="Adam", LastName="Brody", Email="abrody@gmail.com" }
            };

            students.ForEach(s => context.Students.AddOrUpdate(t => t.LastName, s));

            var courses = new List<Course>
            {
                new Course
                {CourseID=1,Title="Tietorakenteet ja algoritmit II",Credits=3 },
                new Course
                {CourseID=2,Title="Laskennan perusmallit", Credits=3 },
                new Course
                {CourseID=4,Title="Liiketoiminnan ja yrittäjyyden perusteet", Credits=5 }
            };

            courses.ForEach(c => context.Courses.AddOrUpdate(p => p.Title, c));

            var enrollments = new List<Enrollment>
            {
                new Enrollment
                {StudentID=students.Single(s=> s.LastName == "Doe").StudentID,Grade=Grade.A,CourseID=courses.Single(c=> c.Title=="Tietorakenteet ja algoritmit 1").CourseID},
                new Enrollment
                {StudentID=students.Single(s=> s.LastName == "Doe").StudentID,Grade=Grade.B,CourseID=courses.Single(c=> c.Title=="Liiketoiminnan ja yrittäjyyden perusteet").CourseID },
                new Enrollment
                {StudentID=students.Single(s=> s.LastName == "Opiskelija").StudentID,Grade=Grade.A , CourseID=courses.Single(c=>c.Title=="Laskennan perusmallit").CourseID}
            };

            enrollments.ForEach(e => context.Enrollments.AddOrUpdate(d => d.EnrollmentID, e));

            var events = new List<Event>
            {
                new Event
                {EventID=1, Title="Bileet",Location="Paikka X", startDate=DateTime.Parse("2014-05-05 17:00"), endDate=DateTime.Parse("2014-06-05 2:00")},
                new Event
                {EventID=2, Title="Tapaaminen", Location="Paikka Y", startDate=DateTime.Parse("2015-01-04 12:00"), endDate=DateTime.Parse("2015-01-04 15:00")}

            };

            events.ForEach(e => context.Events.AddOrUpdate(d => d.EventID, e));

            var attendances = new List<Attendance>
            { 
                new Attendance
                {StudentID=1, EventID=2,isAttending=true },
                new Attendance
                {StudentID=2, EventID=1,isAttending=true },
                new Attendance
                {StudentID=1, EventID=1, isAttending=true },
                new Attendance
                {StudentID=3, EventID=1, isAttending=true }
            };

            attendances.ForEach(a => context.Attendances.AddOrUpdate(b => b.EventID, a));

            foreach(Enrollment e in enrollments)
            {
                var enrollmentInDatabase = context.Enrollments.Where(
                        s =>
                            s.StudentID == e.StudentID && s.Course.CourseID == e.CourseID).SingleOrDefault();
                if (enrollmentInDatabase == null)
                {
                    context.Enrollments.Add(e); 
                }
                
            }

            foreach(Attendance a in attendances)
            {
                var attendanceInDatabase = context.Attendances.Where(
                        s =>
                            s.StudentID == a.StudentID && s.Event.EventID == a.EventID).SingleOrDefault();
                    
                if (attendanceInDatabase == null)
                {
                    context.Attendances.Add(a);
                }
            }
            context.SaveChanges();
  
        }
    }
}
