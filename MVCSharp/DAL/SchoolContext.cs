﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCSharp.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MVCSharp.DAL
{
    public class SchoolContext: DbContext
    {

        public SchoolContext() : base("SchoolContext")
        {
            Database.SetInitializer<SchoolContext>(new CustomInitializer<SchoolContext>());
        }

        public class CustomInitializer<T> : DropCreateDatabaseIfModelChanges<SchoolContext>
        {
            public override void InitializeDatabase(SchoolContext context)
            {
                context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, string.Format("ALTER DATABASE {0} SET SINGLE_USER WITH ROLLBACK IMMEDIATE", context.Database.Connection.Database));
                base.InitializeDatabase(context);
            }   
            protected override void Seed(SchoolContext context)
            {
                base.Seed(context);
            }
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Attendance> Attendances { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}