﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVCSharp.Models
{
    public class Attendance
    {
        [Required]
        public Boolean isAttending { get; set; }
        [Required]
        [Key]
        [Column(Order=1)]
        public int StudentID { get; set; }
        [Required]
        [Key]
        [Column(Order=2)]
        public int EventID { get; set; }
        public virtual Student Student { get; set; }
        public virtual Event Event { get; set; }
    }
}