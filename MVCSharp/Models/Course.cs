﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVCSharp.Models
{
    public class Course
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CourseID { get; set; }
        [Required(ErrorMessage = "Course name is required")]
        [StringLength(40)]
        public string Title { get; set; }
        [Required(ErrorMessage = "Credits are required")]
        public int Credits { get; set; }
        public virtual ICollection<Enrollment> Enrollments { get; set; }


    }

}