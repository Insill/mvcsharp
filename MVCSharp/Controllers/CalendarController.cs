﻿using DayPilot.Web.Mvc;
using DayPilot.Web.Mvc.Events.Calendar;
using DayPilot.Web.Mvc.Enums;
using DayPilot.Web.Mvc.Events.Month;
using MVCSharp.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCSharp.Controllers
{
    public class CalendarController : Controller
    {
        
          // GET:   
        public ActionResult Backend() 
        {
            return new Dpc().CallBack(this); 

        }

        class Dpc: DayPilotCalendar
        {
            SchoolContext db = new SchoolContext();

            protected override void OnCommand(DayPilot.Web.Mvc.Events.Calendar.CommandArgs e)
            {
                switch (e.Command)
                {
                    case "refresh":
                        Update();
                        break;
                }
            }

            protected override void OnEventMove(DayPilot.Web.Mvc.Events.Calendar.EventMoveArgs e)
            {
                var item = (from ev in db.Events where ev.EventID == Convert.ToInt32(e.Id) select ev).First();
                if (item != null)
                {
                    item.startDate = e.NewStart;
                    item.endDate = e.NewEnd;
                    db.SaveChanges();
                }
            }

            protected override void OnEventDelete(DayPilot.Web.Mvc.Events.Calendar.EventDeleteArgs e)
            {
                var item = (from ev in db.Events where ev.EventID == Convert.ToInt32(e.Id) select ev).First();
                db.Events.Remove(item);
                db.SaveChanges();
                Update();
            }


            protected override void OnInit(DayPilot.Web.Mvc.Events.Calendar.InitArgs e)
            {
                UpdateWithMessage("Welcome!", CallBackUpdateType.Full);
            }
            protected override void OnFinish()
            {
                if (UpdateType == CallBackUpdateType.None)
                {
                    return;
                }

                DataIdField = "EventID";
                DataStartField = "startDate";
                DataEndField = "endDate";
                DataTextField = "Title";

                Events = from e in db.Events select e;
            }
        }

    }
}