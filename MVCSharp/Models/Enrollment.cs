﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCSharp.Models
{
    public enum Grade
    {
        A,B,C,D,E,F
    }
    public class Enrollment
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage ="The EnrollmentID cannot be blank")]
        public int EnrollmentID { get; set; }
        [Required(ErrorMessage ="The CourseID cannot be blank")]
        public int CourseID { get; set; }
        [Required(ErrorMessage="The StudentID cannot be blank")]
        public int StudentID { get; set; }
        [Required(ErrorMessage="The grade cannot be blank")]
        public Grade? Grade { get; set; }
        public virtual Course Course { get; set; }
        public virtual Student Student { get; set; }

    }
}