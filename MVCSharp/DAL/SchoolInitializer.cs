﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MVCSharp.Models;
using System.Data.Entity.Validation;
using System.Text;

namespace MVCSharp.DAL
{
    public class SchoolInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<SchoolContext>
    {
        protected override void Seed(SchoolContext context)
        {

            var Students = new List<Student>
            {
                new Student { FirstName="X",LastName="X",StudentID=1,Email="X@hotmail.com"},
                new Student { FirstName="Erkki", LastName="Esimerkki",StudentID=2, Email="esimerkkie@gmail.com" }
            };

            Students.ForEach(s => context.Students.Add(s));
            SaveChanges(context);

            var Courses = new List<Course>
            {
                new Course { CourseID=1, Title="Introduction to Computer Science",Credits=5 },
                new Course { CourseID=2, Title="Introduction to Programming",Credits=4}
            };

            Courses.ForEach(s => context.Courses.Add(s));
            SaveChanges(context);

            var Enrollments = new List<Enrollment>
            {
                new Enrollment {StudentID = 1, CourseID=1, Grade=Grade.A},
                new Enrollment {StudentID = 1, CourseID=2, Grade=Grade.D},
                new Enrollment {StudentID = 2, CourseID=1, Grade=Grade.C},
                new Enrollment {StudentID = 2, CourseID=2, Grade=Grade.B}
            };

            var Events = new List<Event>
            {
                new Event { EventID=1, Title="Rekrypäivät", Location="Technopolis E14/15", startDate = DateTime.Parse("15/3/2016 12:00"), endDate = DateTime.Parse("15/3/2016 16:00"), Description="Tilaisuuteen on tulossa lähiseudun IT-yrityksiä esittelemään toimintaansa ja etsimään uusia tekijöitä joukkoonsa. Tule siis sinäkin ja löydä itsellesi harjoittelu-, projekti- tai kesätyöpaikka!"}
            };

            var Attendances = new List<Attendance>
            {
                new Attendance {StudentID=1, EventID=1, isAttending=true}
            };

            Attendances.ForEach(a => context.Attendances.Add(a));
            Events.ForEach(e => context.Events.Add(e)); 
            Enrollments.ForEach(s => context.Enrollments.Add(s));
            SaveChanges(context);
        }
        private void SaveChanges(DbContext context)
        {
            try
            {
                context.SaveChanges();
            }

            catch (DbEntityValidationException ex)
            {
                StringBuilder sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }
                throw new DbEntityValidationException(
                    "Entity Validation Failed: Errors:\n" + sb.ToString(), ex);
            }
        }
    }
}